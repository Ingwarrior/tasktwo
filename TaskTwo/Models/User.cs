﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskTwo.Models
{
    public class User
    {
        public int Id {get;set;}
        [Required(ErrorMessage = "User name is required")]
        public string Username {get;set;}
        [Required(ErrorMessage = "User surname is required")]
        public string Surname {get;set;}
        public DateTime DateRegister {get;set;}
        [Required(ErrorMessage = "User email is required")]
        public string Email { get; set; }

        
        public virtual ICollection<Order> Orders { get; set; }
        public User()
        {
            Orders = new List<Order>();
        }

}
}