﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskTwo.Models
{
    public class Order
    {
        public int Id {get;set;}
        public DateTime DatePurchase { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public Order()
        {
            Products = new List<Product>();
        }

    }
}