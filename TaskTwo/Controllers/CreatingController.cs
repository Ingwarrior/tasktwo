﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskTwo.Models;
using System.Net.Http;

namespace TaskTwo.Controllers
{
    public class CreatingController : Controller
    {
        private static readonly HttpClient client = new HttpClient();

        // GET: Creating
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(User user)
        {
            user.DateRegister = DateTime.Now;

            using (PurchaseContext db = new PurchaseContext())
            {
                if (ModelState.IsValid)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    ViewBag.Status = "Added user";
                    return View("Index");
                }
                else
                {
                    ViewBag.Status = "Error";
                    return View("Index");
                }
            }
        }

        [HttpPost]
        public ActionResult CreateProduct(Product product)
        {
            product.DateUpd = DateTime.Now;

            using (PurchaseContext db = new PurchaseContext())
            {
                if (ModelState.IsValid)
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    ViewBag.Status = "Added product";
                    return View("Index");
                }
                else
                {
                    ViewBag.Status = "Error";
                    return View("Index");
                }

            }
        }

        [HttpPost]
        public ActionResult CreateOrder()
        {
            Order order = new Order();

            using (PurchaseContext db = new PurchaseContext())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        string username = Request["Email"];
                        string productname = Request["Productname"];

                        var user_order = db.Users.Where(t => t.Email == username).First();
                        var products = db.Products.Where(t => t.ProductName == productname).ToList();

                        order.DatePurchase = DateTime.Now;
                        order.User = user_order;
                        order.Products = products;

                        db.Orders.Add(order);
                        db.SaveChanges();

                        ViewBag.Status = "Order added";
                        return View("Index");
                    }
                    else
                    {
                        ViewBag.Status = "Error";
                        return View("Index");
                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                return View("Error");
            }
        }
    }
}
